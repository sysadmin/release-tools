#!/usr/bin/env bash

function finish {
    deactivate &> /dev/null
}
trap finish EXIT

deactivate &> /dev/null
dir="$(dirname "$(readlink -f $0)")"
rm -rf "$dir/venv"
python3 -m venv "$dir/venv"
. "$dir/venv/bin/activate"
pip install --upgrade "$dir" &> /dev/null

KDE_RELEASE_DIR="$(dirname "$(dirname "$dir")")" "$1" "${@:2}"
