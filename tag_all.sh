#!/bin/bash

unset CDPATH

here=$PWD

# We look for the checkouts in subdirs (frameworks/, kdesupport/) of $srcdir
# TODO: adapt this for KDE SC releases
srcdir=/d/kde/src/5

if [ ! -d $srcdir ]; then
    echo "$srcdir does not exist, please fix srcdir variable"
    exit
fi

cat $here/modules.git | while read repoid branch; do
    cd $srcdir || exit 1
    echo $repoid
    . $here/version
    tagname=v$version
    versionfile=$here/versions/$repoid
    if [ ! -f $versionfile ]; then echo "$versionfile not found"; exit 1; fi
    b=`sed '2q;d' $versionfile`
    echo $b
    if [ -d frameworks/$repoid ]; then
        cd frameworks/$repoid
    elif [ -d kdesupport/$repoid ]; then
        cd kdesupport/$repoid || exit 2
    elif [ -d $repoid ]; then
        cd $repoid || exit 2
    else
        echo "NOT FOUND: $repoid"
        exit 3
    fi
    echo $PWD
    git fetch || exit 2
    git tag -s -a $tagname $b -m "Create tag for $version"  || exit 4
    git push --tags -o ci.skip || exit 5
done
