#!/bin/sh

set -e
VERSION_STABLE="5.6.90"
VERSION_MASTER="5.7.40"

i=~/kdepim-dev/src/kde/pim/kdepim-runtime
cd $i
name=`basename $i`
echo $name
git checkout master
git reset --hard origin/master
git merge origin/Applications/17.12
git checkout Applications/17.12
git reset --hard origin/Applications/17.12
git pull
sed -e  "s/KDEPIM_RUNTIME_VERSION_NUMBER \".*\"/KDEPIM_RUNTIME_VERSION_NUMBER \"${VERSION_STABLE}\"/" -i CMakeLists.txt
git add CMakeLists.txt
git commit -m "GIT_SILENT: Prepare ${VERSION_STABLE}"
git checkout master
git reset --hard origin/master
sed -e  "s/KDEPIM_RUNTIME_VERSION_NUMBER \".*\"/KDEPIM_RUNTIME_VERSION_NUMBER \"${VERSION_MASTER}\"/" -i CMakeLists.txt
git add CMakeLists.txt
git commit -m "GIT_SILENT: Open for future 5.8.x"
git merge Applications/17.12 -s ours -m "Merge Applications/17.12"
git push origin Applications/17.12 master

i=~/kdepim-dev/src/kde/pim/syndication
cd $i
name=`basename $i`
echo $name
git checkout master
git pull
git reset --hard origin/master
git merge origin/Applications/17.12
git checkout Applications/17.12
git reset --hard origin/Applications/17.12
git pull
sed -e  "s/SYNDICATION_LIB_VERSION \".*\"/SYNDICATION_LIB_VERSION \"${VERSION_STABLE}\"/" -i CMakeLists.txt
git add CMakeLists.txt
git commit -m "GIT_SILENT: Prepare ${VERSION_STABLE}"
git checkout master
git reset --hard origin/master
sed -e  "s/SYNDICATION_LIB_VERSION \".*\"/SYNDICATION_LIB_VERSION \"${VERSION_MASTER}\"/" -i CMakeLists.txt
git add CMakeLists.txt
git commit -m "GIT_SILENT: Open for future 5.8.x"
git merge Applications/17.12 -s ours -m "Merge Applications/17.12"
git push origin Applications/17.12 master
