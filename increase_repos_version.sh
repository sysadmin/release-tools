#!/bin/bash

unset CDPATH

here=$PWD

cmd=
if [ "$dry_run" = "1" ]; then
    cmd=echo
fi

srcdir=/home/kdeunstable
if [ ! -d $srcdir ]; then
    echo "$srcdir does not exist, please fix srcdir variable"
    exit 1
fi

cat $here/modules.git | while read repoid branch; do
    echo $repoid
    . $here/version
    checkout=$srcdir/$repoid
    cd $checkout || exit 2
    echo $PWD
    $cmd git checkout $branch || exit 3
    $cmd git pull --rebase || exit 4
    $cmd git diff --exit-code origin/$branch || exit 7
    version_major=`echo $version | cut -d. -f1`
    version_minor=`echo $version | cut -d. -f2`
    version_patch=`echo $version | cut -d. -f3`
    $cmd perl -pi -e '$_ =~ s/\Q$1/'$version_major'/ if (/^set ?\(KDE_APPLICATIONS_VERSION_MAJOR "([0-9]*)"/);' CMakeLists.txt
    $cmd perl -pi -e '$_ =~ s/\Q$1/'$version_minor'/ if (/^set ?\(KDE_APPLICATIONS_VERSION_MINOR "([0-9]*)"/);' CMakeLists.txt
    $cmd perl -pi -e '$_ =~ s/\Q$1/'$version_patch'/ if (/^set ?\(KDE_APPLICATIONS_VERSION_MICRO "([0-9]*)"/);' CMakeLists.txt
    $cmd perl -pi -e '$_ =~ s/\Q$1/'$version_major'/ if (/^set ?\(RELEASE_SERVICE_VERSION_MAJOR "([0-9]*)"/);' CMakeLists.txt
    $cmd perl -pi -e '$_ =~ s/\Q$1/'$version_minor'/ if (/^set ?\(RELEASE_SERVICE_VERSION_MINOR "([0-9]*)"/);' CMakeLists.txt
    $cmd perl -pi -e '$_ =~ s/\Q$1/'$version_patch'/ if (/^set ?\(RELEASE_SERVICE_VERSION_MICRO "([0-9]*)"/);' CMakeLists.txt
    $cmd git commit -a -m "GIT_SILENT Upgrade release service version to $version."
    $cmd git pull --rebase || exit 5
    $cmd git push origin $branch || exit 6
done
