#!/usr/bin/env python3

import os
import subprocess

def getVersionFrom(repo):
	f = open(versionsDir + '/' + repo)
	return f.readlines()[1].strip()
	

f = open('modules.git')
#srcdir="/d/kde/src/5/"
srcdir="/data/devel/kde/release/"
repos=[]

for line in f:
	line = line[:line.find(" ")]
	repos.append(line)

repos.sort()

versionsDir = os.getcwd() + "/versions"

for repo in repos:
	toVersion = getVersionFrom(repo)
	os.chdir(srcdir+repo)

	fromVersion = "v20.04.2"

	p = subprocess.Popen('git fetch', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	retval = p.wait()
	if retval != 0:
		raise NameError('git fetch failed')

	p = subprocess.Popen('git rev-parse '+fromVersion, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	retval = p.wait()
	if retval != 0:
		print('{{{{< details id="{repo}" title="{repo}" link="https://commits.kde.org/{repo}" >}}}}'.format(repo=repo))
		print("+ New in this release")
		print("{{< /details >}}")
		continue

	p = subprocess.Popen('git diff '+fromVersion+'..'+toVersion, shell=True, stdout=subprocess.PIPE)
	diffOutput = p.stdout.readlines()
	retval = p.wait()
	if retval != 0:
		raise NameError('git diff failed', repo, fromVersion, toVersion)

	if len(diffOutput):
		p = subprocess.Popen('git log '+fromVersion+'..'+toVersion, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
		commit = []
		commits = []
		for line in p.stdout.readlines():
			if line.startswith("commit"):
				if len(commit) > 1 and not ignoreCommit:
					commits.append(commit)
				commitHash = line[7:].strip()
				ignoreCommit = False
				commit = [commitHash]
			elif line.startswith("Author"):
				pass
			elif line.startswith("Date"):
				pass
			elif line.startswith("Merge"):
				pass
			else:
				line = line.strip()
				if line.startswith("Merge remote-tracking branch"):
					ignoreCommit = True
				elif line.startswith("SVN_SILENT"):
					ignoreCommit = True
				elif line.startswith("GIT_SILENT"):
					ignoreCommit = True
				elif line.startswith("Merge branch"):
					ignoreCommit = True
				elif line:
					commit.append(line)
		# Add the last commit
		if len(commit) > 1 and not ignoreCommit:
			commits.append(commit)
		
		if len(commits):
			print('{{{{< details id="{repo}" title="{repo}" link="https://commits.kde.org/{repo}" >}}}}'.format(repo=repo))
			for commit in commits:
				extra = ""
				clog = ""
				changelog = commit[1]
				
				for line in commit:
					if line.startswith("BUGS:"):
						bugNumbers = line[line.find(":") + 1:].strip()
						for bugNumber in bugNumbers.split(","):
							if bugNumber.isdigit():
								extra += " Fixes bug [#{0}](https://bugs.kde.org/{0}).".format(bugNumber)
					elif line.startswith("BUG:"):
						bugNumber = line[line.find(":") + 1:].strip()
						if bugNumber.isdigit():
							extra += " Fixes bug [#{0}](https://bugs.kde.org/{0}).".format(bugNumber)
					elif line.startswith("REVIEW:"):
						reviewNumber = line[line.find(":") + 1:].strip()
						extra += " Code review [#{0}](https://git.reviewboard.kde.org/r/{0}).".format(reviewNumber)
					elif line.startswith("CCBUG:"):
						bugNumber = line[line.find(":") + 1:].strip()
						extra += " See bug [#{0}](https://bugs.kde.org/{0}).".format(bugNumber)
					elif line.startswith("FEATURE:"):
						feature = line[line.find(":") + 1:].strip()
						if feature.isdigit():
							extra += " Implements feature [#{0}](https://bugs.kde.org/{0}).".format(feature)
						else:
							if feature and not clog:
								changelog = feature
					elif line.startswith("CHANGELOG:"):
						clog = line[line.find(":") + 1:].strip()
						if clog:
							changelog = clog
				
				commitHash = commit[0]
				if not changelog.endswith("."):
					changelog = changelog + "."
				capitalizedChangelog = changelog[0].capitalize() + changelog[1:]
				print("+ {} [Commit](http://commits.kde.org/{}/{}).{}".format(capitalizedChangelog, repo, commitHash, extra))

			print("{{< /details >}}")
		retval = p.wait()
		if retval != 0:
			raise NameError('git log failed', repo, fromVersion, toVersion)
