#!/usr/bin/awk -f

BEGIN {
    open_handbook = 0;
}

{
    full_line = $0;
    kdoctools_create_handbook_name = "kdoctools_create_handbook";
    lowerinput = tolower(full_line);
    #print ("input: " lowerinput);
    if (lowerinput ~ kdoctools_create_handbook_name) {
         position = index(lowerinput, kdoctools_create_handbook_name);
         position = position + length(kdoctools_create_handbook_name);
         #print("found " substr(full_line, position));
         open_handbook = 1;
    }
    # it could be from the previous finding, or it could come from a previous line
    if (open_handbook == 1) {
        found = match(lowerinput, /[Ss][Uu][Bb][Dd][Ii][Rr] ([^\ )]+)/, subdir);
        if (found) {
            #print("found subdir", subdir[1]);
            print(subdir[1]);
            open_handbook = 0;
        }
    }
}


END {
}
